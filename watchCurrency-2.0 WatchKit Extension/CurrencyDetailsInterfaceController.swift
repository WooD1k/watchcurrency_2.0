//
//  CurrencyDetailsInterfaceController.swift
//  watchCurrency
//
//  Created by Alexey Chulochnikov on 29.04.15.
//  Copyright (c) 2015 Alexey Chulochnikov. All rights reserved.
//

import WatchKit
import Foundation


class CurrencyDetailsInterfaceController: WKInterfaceController {
    @IBOutlet weak var baseCurrencyName: WKInterfaceLabel!
    @IBOutlet weak var exchangeCurrencyName: WKInterfaceLabel!
    @IBOutlet weak var buyExchangeRate: WKInterfaceLabel!
    @IBOutlet weak var sellExchangeRate: WKInterfaceLabel!
    @IBOutlet weak var favoriteSwitch: WKInterfaceSwitch!
    
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    var favCurrencyKey = "favoriteCurrency"
    var favCurrencyObjectKey = "favCurrencyObject"
    var currencyName = ""
    var currencyObject = ["": ""]

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        if let currencyObject = context as? [String: String]{
            self.currencyObject = currencyObject
            
            let exchangeCurrencyName = currencyObject["ccy"]!
            
            self.currencyName = exchangeCurrencyName

            if let favCurrency = userDefaults.objectForKey(self.favCurrencyKey) as? String {
                favoriteSwitch.setOn(favCurrency == self.currencyName)
            }
            
            self.setTitle(exchangeCurrencyName)

            self.baseCurrencyName.setText(currencyObject["base_ccy"])
            self.exchangeCurrencyName.setText(exchangeCurrencyName)
            self.buyExchangeRate.setText(currencyObject["buy"])
            self.sellExchangeRate.setText(currencyObject["sale"])
        }
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    @IBAction func favoriteSwitchValueChanged(value: Bool) {
        // delete previous value when switch value was changed
        userDefaults.removeObjectForKey(favCurrencyKey)
        userDefaults.removeObjectForKey(favCurrencyObjectKey)
        
        // save our currency object and currency name to user defaults
        if (value) {
            userDefaults.setObject(self.currencyName, forKey: favCurrencyKey)
            userDefaults.setObject(self.currencyObject, forKey: favCurrencyObjectKey)
        }
        
        // synchronize user defaults to save our changes
        userDefaults.synchronize()
    }
}
