//
//  InterfaceController.swift
//  watchCurrency-2.0 WatchKit Extension
//
//  Created by Alexey Chulochnikov on 13.01.16.
//  Copyright © 2016 wood1k. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {
    @IBOutlet weak var currencyTable: WKInterfaceTable!
    @IBOutlet var lastUpdatedLabel: WKInterfaceLabel!
    
    var sessionDataTask: NSURLSessionDataTask?
    
    let numberFormatter = NSNumberFormatter()
    
    var currencyArray: NSArray = [];
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
        getExchangeRate()
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    @IBAction func refreshClicked() {
        getExchangeRate()
    }
    
    func getExchangeRate () {
        let requestURL = NSURL(string: "https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5")!
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        let request = NSURLRequest(URL: requestURL)
        sessionDataTask = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
            if error == nil {
                let currencies = (try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)) as! NSArray
                
                self.currencyArray = currencies
                
                self.reloadTableWithData(currencies)
            }
        })
        
        sessionDataTask!.resume()
    }
    
    // func to reload table view content
    func reloadTableWithData (data: NSArray) {
        currencyTable.setNumberOfRows(data.count, withRowType: "CurrencyRow")
        
        for (index, currency) in data.enumerate() {
            if let row = currencyTable.rowControllerAtIndex(index) as? CurrencyRow {
                self.numberFormatter.numberStyle = NSNumberFormatterStyle.NoStyle
                self.numberFormatter.maximumFractionDigits = 2
                
                let currencyName = currency.objectForKey("ccy") as! String
                
                let sellPriceString = currency.objectForKey("sale") as! String
                let buyPriceString = currency.objectForKey("buy") as! String
                
                let sellPrice = self.numberFormatter.numberFromString(sellPriceString)!.floatValue
                let buyPrice = self.numberFormatter.numberFromString(buyPriceString)!.floatValue
                
                row.currencyName.setText("\(currencyName)/UAH")
                row.buyExchangeRate.setText("\(buyPrice)")
                row.sellExchangeRate.setText("\(sellPrice)")
            }
        }
        
        let date = NSDate()
        let dateFormatter = NSDateFormatter()
        dateFormatter.timeStyle = .ShortStyle
        let lastUpdated = dateFormatter.stringFromDate(date)
        
        lastUpdatedLabel.setText("Last update:\n\(lastUpdated)");
    }
    
    // func to pass data to controller by segue with identifier
    override func contextForSegueWithIdentifier(segueIdentifier: String, inTable table: WKInterfaceTable, rowIndex: Int) -> AnyObject? {
        if segueIdentifier == "CurrencyDetails" {
            let currency = currencyArray[rowIndex] as? [String : String]
            return currency
        }
        
        return nil
    }
    
    // func to open details controller from glance
    override func handleUserActivity(userInfo: [NSObject : AnyObject]!) {
        if let currencyObject = userInfo["currencyObject"] as? [String: String] {
            pushControllerWithName("CurrencyDetailsInterfaceController", context: currencyObject)
        }
    }
}
