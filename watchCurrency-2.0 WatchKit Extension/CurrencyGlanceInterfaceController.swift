//
//  CurrencyGlanceInterfaceController.swift
//  watchCurrency
//
//  Created by Alexey Chulochnikov on 04.05.15.
//  Copyright (c) 2015 Alexey Chulochnikov. All rights reserved.
//

import WatchKit
import Foundation


class CurrencyGlanceInterfaceController: WKInterfaceController {
    @IBOutlet weak var favCurrencyName: WKInterfaceLabel!
    @IBOutlet weak var favCurrencyExchangeRate: WKInterfaceLabel!
    @IBOutlet weak var baseCurrencyName: WKInterfaceLabel!

    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
        if let favCurrency = userDefaults.valueForKey("favoriteCurrency") as? String {
            let favCurrencyObject = userDefaults.objectForKey("favCurrencyObject") as! NSDictionary
            let sellRate = favCurrencyObject.valueForKey("sale") as! String
        
            favCurrencyName.setText("BUY \(favCurrency)")
            favCurrencyExchangeRate.setText(sellRate)
            updateUserActivity("com.wood1k.watchCurrency.glance", userInfo: ["currencyObject": favCurrencyObject], webpageURL: nil)
        } else {
            favCurrencyName.setText("")
            favCurrencyExchangeRate.setText("fav isn't")
            baseCurrencyName.setText("selected")
        }
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
