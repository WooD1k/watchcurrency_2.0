//
//  CurrencyRow.swift
//  watchCurrency
//
//  Created by Alexey Chulochnikov on 18.04.15.
//  Copyright (c) 2015 Alexey Chulochnikov. All rights reserved.
//

import Foundation
import WatchKit

class CurrencyRow: NSObject {
    @IBOutlet weak var currencyName: WKInterfaceLabel!
    @IBOutlet weak var buyExchangeRate: WKInterfaceLabel!
    @IBOutlet weak var sellExchangeRate: WKInterfaceLabel!
}